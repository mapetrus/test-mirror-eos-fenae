using System.Collections;
using System.Collections.Generic;
using EpicTransport;
using UnityEngine;
using UnityEngine.UI;

public class TestsFenae : MonoBehaviour
{
    [SerializeField] private EOSSDKComponent _Component;
    [SerializeField] private InputField _Input;
    

    public void SetCredential()
    {
        _Component.devAuthToolCredentialName = _Input.text;
        EOSSDKComponent.Initialize();
    }
}
